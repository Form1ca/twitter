module.exports = {
  "extends": "airbnb-base",
   "rules": {
     // "comma-dangle": ["error", "never"],
     //"treatUndefinedAsUnspecified": false,
     "prefer-destructuring": ["error", {
      "VariableDeclarator": {
        "array": false,
        "object": false
      },
      "AssignmentExpression": {
        "array": true,
        "object": true
      }
    }, {
      "enforceForRenamedProperties": false
    }]
   }
  };