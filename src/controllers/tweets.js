import to from 'await-to-js';
import Twitter from 'twitter';
import config from '../config';
import twitterLocal from '../services/twitterlocal';


const twitterClient = new Twitter(config.tweetsConfig);

const returnStatus = (ctx, code = 200, body = '') => {
  ctx.code = code;
  ctx.body = body;
  return ctx;
};

const searchTweets = async tag => new Promise((resolve, reject) => {
  twitterClient.get('search/tweets', { q: tag }, (error, tweets) => {
    if (error) {
      console.log(error);
      reject(error);
    }
    resolve(tweets);
  });
});


const searchByTag = async (ctx) => {
  const tag = ctx.request.query.tag;
  if (!tag) {
    returnStatus(ctx, 400, 'Parameter \'tweet\' not found.');
    return;
  }


  const [err, tweets] = await to(searchTweets(tag));
  if (err || !tweets) {
    returnStatus(ctx, 204);
    return;
  }


  returnStatus(ctx, 200, tweets);
};


const saveTweets = async (ctx) => {

  const tweet = ctx.request.body;
  if (!tweet) {
    returnStatus(ctx, 400, 'Parameter not found.');
    return;
  }


  const [err, res, code] = await twitterLocal.saveTweets(tweet);
  if (err || !res || code !== 200) {
    returnStatus(ctx, 500, 'Error save tweet');
    return;
  }


  returnStatus(ctx, 200, 'OK');
};


const rateSavedTwits = async (ctx) => {

  const id = ctx.params.id;
  const rating = ctx.params.rating;
  if (!id || !rating) {
    returnStatus(ctx, 400, 'Parameter not found.');
    return;
  }


  const [err, res, code] = await twitterLocal.rateSavedTwits(id, rating);
  if (err || !res || code !== 200) {
    returnStatus(ctx, 500, 'Error rating tweet');
    return;
  }


  returnStatus(ctx, 200, 'OK');
};


const getSavedTwits = async (ctx) => {

  const [err, res, code] = await twitterLocal.getSavedTwits(ctx.request.query);
  if (err || !res || code !== 200) {
    returnStatus(ctx, 500, 'Error get saved twits');
    return;
  }


  returnStatus(ctx, 200, res);
};


export default {
  searchByTag,
  getSavedTwits,
  saveTweets,
  rateSavedTwits,
};
