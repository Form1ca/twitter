import Router from 'koa-router';
import tweets from '../../controllers/tweets';


const router = new Router();

router
  .get('/tweets/search', tweets.searchByTag)
  .post('/tweets/', tweets.saveTweets)
  .put('/tweets/:id/rate/:rating', tweets.rateSavedTwits)
  .get('/tweets/', tweets.getSavedTwits);

export function routes() { return router.routes(); }
export function allowedMethods() { return router.allowedMethods(); }
