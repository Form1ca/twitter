import config from '../../config';

const onerror = (err, ctx) => {
  console.log(config.serviceName, err, ctx);
};

export default onerror;
