import Koa from 'koa';
import bodyparser from 'koa-bodyparser';
import logger from 'koa-logger';
import { routes, allowedMethods } from './routes';
import onerror from './middlewares/onerror';
import config from '../config';

const app = new Koa();

app
  .on('error', onerror)
  .use(logger())
  .use(allowedMethods())
  .use(bodyparser())
  .use(routes());


app.listen(config.port, () => {
  console.log('%s service start listening at port %d', config.serviceName, config.port);
});
