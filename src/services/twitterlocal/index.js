import request from 'request-promise';
import config from '../../config';

const baseUri = config.services.twitterLocal;

// запрос к twitter-local сохраняем твиты в базу
const saveTweets = async (tweet) => {
  try {
    const options = {
      json: true,
      uri: `${baseUri}/tweets/`,
      body: tweet,
      headers: {
        service: config.serviceName,
      },
      resolveWithFullResponse: true,
      method: 'POST',
    };
    const res = await request(options);
    return [null, res.body, res.statusCode];
  } catch (error) {
    console.log(`${config.services.twitterLocal}/`, error);
    return [error];
  }
};

// запрос к twitter-local оценка сохранненых твитов
const rateSavedTwits = async (id, rating) => {
  try {
    const options = {
      json: true,
      uri: `${baseUri}/tweets/${id}/rate/${rating}`,
      headers: {
        service: config.serviceName,
      },
      resolveWithFullResponse: true,
      method: 'PUT',
    };
    const res = await request(options);

    return [null, res.body, res.statusCode];
  } catch (error) {
    console.log(`${config.services.twitterLocal}/`, error);
    return [error];
  }
};

// запрос к twitter-local получаем сохраненные твиты с оценками и пагинацией
const getSavedTwits = async (getParams) => {
  try {
    const options = {
      json: true,
      uri: `${baseUri}/tweets/`,
      headers: {
        service: config.serviceName,
      },
      qs: getParams,
      resolveWithFullResponse: true,
      method: 'GET',
    };
    const res = await request(options);
    return [null, res.body, res.statusCode];
  } catch (error) {
    console.log(`${config.services.twitterLocal}/`, error);
    return [error];
  }
};

export default {
  saveTweets,
  rateSavedTwits,
  getSavedTwits,
};
