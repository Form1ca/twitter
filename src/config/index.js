const env = process.env;
const host = env.SERVICE_HOST || 'localhost';
const environment = env.NODE_ENV || 'development';
const serviceName = env.SERVICE_NAME || 'Twitter';
import tweetsConfig from './.secret';


const defaultConfig = {
  environment,
  serviceName,
  host,
  tweetsConfig,
};


const config = {
  development: {
    port: env.SERVICE_PORT || 10020,
    services: {
      twitterLocal: env.SERVICES_TWITTER_LOCAL_URI || 'http://localhost:10021',
    },
  },
  test: {
    port: env.SERVICE_PORT || 10020,
    services: {
      twitterLocal: env.SERVICES_TWITTER_LOCAL_URI || 'http://localhost:10021',
    },
  },
  production: {
    port: env.SERVICE_PORT || 10020,
    services: {
      twitterLocal: env.SERVICES_TWITTER_LOCAL_URI || 'http://192.168.0.100:10021',
    },
  },

};


export default { ...defaultConfig, ...config[environment] };
